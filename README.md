# BeAGLE2lund

BeAGLE2lund is a tool to convert BeAGLE raw output into lund file readable by gemc.

## Prerequisites

- Root

## Installing

```
g++ BeAGLE2lund.cxx \`root-config --libs --cflags --evelibs\` -o BeAGLE2lund
```

## Running

```
./BeAGLE2lund <input file without the file extension>
```

Example for file named : BeAGLEoutput.txt

```
./BeAGLE2lund BeAGLEoutput
```

Converted file name : BeAGLEoutput_lund.dat
