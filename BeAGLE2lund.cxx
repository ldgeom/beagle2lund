// BeAGLE output to gemc lund file converter
// 
// Compilation : g++ BeAGLE2lund.cxx `root-config --libs --cflags --evelibs` -o BeAGLE2lund
//
// Use : ./BeAGLE2lund <input file without the file extension>
//     for example : - BeAGLE output file : BeAGLEoutput.txt
//                   - Program execution : ./BeAGLE2lund BeAGLEoutput

//C++
#include <iostream>
#include <map>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
//Root
#include "TString.h"
#include "TDatabasePDG.h"
#include "TParticlePDG.h"

using namespace std;

int main(int argc, char* argv[])
{

  TString infilebase = argv[1];

  TString infile  = infilebase + ".txt";
  TString outfile = infilebase + "_lund.dat";

  ifstream in;
  in.open(infile);
  ofstream out;
  out.open(outfile);
  

  string line;
  char word1[1000];

  Int_t     tmpVar;

  Int_t     beamID, zTarget, aTarget;
  Double_t  beamEnergy;
  TString   header;
  Int_t     iParticle = 0;

  Double_t  ksCode, pID;
  Double_t  pX, pY, pZ, pE, pM, vX, vY, vZ;

  vector< vector<Double_t> > events;


  for(Int_t i=0; i<5; i++)
    getline(in,line,'\n');
    
  while(in.good()) {

   in >> word1;

    if(word1[0] == '=') {

      if (iParticle != 0) {

        int entries1 = events.size() ;
        
        out << entries1 << " "
            << aTarget  << " "
            << zTarget  << " "
            << "0.0"    << " "
            << "0.0"    << " "
            << "0.0"    << " "
            << "0.0"    << " "
            << "0.0"    << " "
            << "0.0"    << " "
            << "0.0"    << endl;
            
        for(int i=0;i<entries1; i++) {

          out << "  " ;

          int entries2 = events[i].size();

          for (int k=0;k<entries2; k++) {

            if (k<6)
              out << (int)events[i][k] << " ";

            else
              out << fixed << setprecision(5) << events[i][k] << " ";
          }

          out << endl;

        }

        events.clear();
        iParticle=0;
      }


      getline(in,line,'\n');
      in >> tmpVar >> tmpVar  >> tmpVar;
      in >> beamID >> aTarget >> zTarget >> beamEnergy;
      getline(in,line,'\n');
      getline(in,line,'\n');


      if(!in.eof()) {

        for(int m=0; m<2; m++) {

          in >> tmpVar >> ksCode >> pID;
          in >> tmpVar >> tmpVar >> tmpVar >> tmpVar;
          in >> pX     >> pY     >> pZ     >> pE;
          in >> pM     >> vX     >> vY     >> vZ;

          TParticlePDG *particle = TDatabasePDG::Instance()->GetParticle(pID);

          events.push_back(vector<Double_t>());
          events[iParticle].push_back(iParticle+1);
          events[iParticle].push_back(particle->Charge()/3);
          events[iParticle].push_back(ksCode);
          events[iParticle].push_back(pID);
          events[iParticle].push_back(0);
          events[iParticle].push_back(0);
          events[iParticle].push_back(pX);
          events[iParticle].push_back(pY);
          events[iParticle].push_back(pZ);
          events[iParticle].push_back(pE);
          events[iParticle].push_back(pM);
          events[iParticle].push_back(vX);
          events[iParticle].push_back(vY);
          events[iParticle].push_back(vZ);
          getline(in,line,'\n');

          iParticle++;

        }

      }
        
    }

    else {

      in >> ksCode >> pID;
      in >> tmpVar >> tmpVar >> tmpVar >> tmpVar;
      in >> pX     >> pY     >> pZ     >> pE;
      in >> pM     >> vX     >> vY     >> vZ;
      getline(in,line,'\n');

      if (ksCode == 1 && pID != 80000) {

        TParticlePDG *particle = TDatabasePDG::Instance()->GetParticle(pID);

        events.push_back(vector<Double_t>());
        events[iParticle].push_back(iParticle+1);
        events[iParticle].push_back(particle->Charge()/3);
        events[iParticle].push_back(ksCode);
        events[iParticle].push_back(pID);
        events[iParticle].push_back(0);
        events[iParticle].push_back(0);
        events[iParticle].push_back(pX);
        events[iParticle].push_back(pY);
        events[iParticle].push_back(pZ);
        events[iParticle].push_back(pE);
        events[iParticle].push_back(pM);
        events[iParticle].push_back(vX);
        events[iParticle].push_back(vY);
        events[iParticle].push_back(vZ);

        iParticle++;
      }

     }

  }

  in.close();
  out.close();

  return 0;
}
